
<!-- Modal -->
<div class="modal fade" id="modalid" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
{{--            <form id='formid'>--}}
            <div class="modal-body">
                ....
{{--                    @csrf--}}
{{--                    @method('PUT')--}}

{{--                    <div class="row">--}}
{{--                        <div class="col-xs-12 col-sm-12 col-md-12">--}}
{{--                            <div class="form-group">--}}
{{--                                <strong>Player Name:</strong>--}}
{{--                                <input type="text" name="player_name" class="form-control" placeholder="Enter player's role" value="{{$post->player_name}}">--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                        <div class="col-xs-12 col-sm-12 col-md-12">--}}
{{--                            <div class="form-group">--}}
{{--                                <strong>Role:</strong>--}}
{{--                                <input type="text" name="role" class="form-control" placeholder="Enter player's role" value="{{$post->role}}">--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                        <div class="col-xs-12 col-sm-12 col-md-12">--}}
{{--                            <div class="form-group">--}}
{{--                                <strong>Age:</strong>--}}
{{--                                <input type="text" name="age" class="form-control" placeholder="Enter player age" value="{{$post->age}}">--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                    </div>--}}
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary">Save changes</button>
            </div>
{{--            </form>--}}
        </div>
    </div>
</div>
<div class="container">
    <div class="jumbotron">
        <div class="row">
            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modalid">
                edit
            </button>
        </div>
    </div>
</div>
<!-- Button trigger modal -->

<script src="https://code.jquery.com/jquery-3.6.0.js" integrity="sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk=" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

{{--<script type="text/javascript">--}}
{{--    $(document).ready(function(){--}}
{{--        $('formid').on('submit',function(e){--}}
{{--            e.preventDefault();--}}
{{--            $.ajax({--}}
{{--                type:"POST",--}}
{{--                url: "route('posts.update',$post->id)",--}}
{{--                data:$('formid').serialize(),--}}
{{--                success:function(response){--}}
{{--                    console.log(response)--}}
{{--                    $('modalid').modal('hide')--}}
{{--                    alert("Data Saved");--}}
{{--                },--}}
{{--                error:function(error){--}}
{{--                    console.log(error)--}}
{{--                }--}}
{{--            });--}}
{{--        });--}}
{{--    });--}}
{{--</script>--}}
