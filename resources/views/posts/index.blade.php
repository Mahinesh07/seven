@extends('posts.layout')

@section('content')
    <div class="row" style="margin-top: 5rem;">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Laravel 8 CRUD</h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-success" href="{{ route('posts.create') }}">
                    <i class="fas fa-plus-circle"></i>
{{--                    <span class="material-icons-outlined">--}}
{{--                        create--}}
{{--                    </span>--}}
                </a>
            </div>
        </div>
    </div>

{{--    @if ($message = Session::get('success'))--}}
{{--        <div class="alert alert-success">--}}
{{--            <p>{{ $message }}</p>--}}
{{--        </div>--}}
{{--    @endif--}}

    <table class="table table-stripped">
        <tr>
            <th>No</th>
            <th>Name</th>
            <th>Role</th>
            <th>Age</th>
            <th>Comments</th>
            <th width="280px">Action</th>
        </tr>
        @foreach ($data as $key => $value)

            <tr>
                <td>{{ ++$i }}</td>
                <td>{{ $value->player_name }}</td>
                <td>{{ $value->role }}</td>
                <td>{{ $value->age}}</td>
                <td>{{ $comment[$key]->comments }}</td>
                <td>
                    <form action="{{ route('posts.destroy',$value->id) }}" method="POST">
                        <a href="{{ route('posts.show',$value->id) }}">
                            <i class="fas fa-eye text-info fa-lg"></i>
                        </a>

                        @csrf
                        @method('DELETE')
                        <button type="submit" style="border: none; background-color:transparent;" >
                            <i class="fas fa-trash fa-lg text-danger"></i>
                        </button>
                    </form>
                </td>
            </tr>

{{--        @foreach ($comment as $key => $value)--}}
{{--            <tr>--}}
{{--                --}}
{{--            </tr>--}}
        @endforeach
    </table>
    {{ $data->links('pagination::bootstrap-4')}}
@endsection
