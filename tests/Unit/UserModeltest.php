<?php

namespace Tests\Unit;

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use App\Models\User;
class UserModeltest extends TestCase
{
    use DatabaseTransactions;
    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function test_example()
    {
        //$this->assertTrue(true);
        $user=User::create(['name'=>'dhanush', 'email'=>'dhanush@gmaail.com', 'password'=>'password']);
        $this->assertEquals('dhanush@gmaail.com', $user->email);
    }
}
