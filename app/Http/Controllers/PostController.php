<?php

namespace App\Http\Controllers;

use App\Models\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Comment;
class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //
        //var $data;
        //$data = Post::paginate(5);
//        if ($request->ajax()) {
//            $data = Post::select('*');
//            return Datatables::of($data)
//                ->make(true);
//        }
        $data=Post::paginate(5);
        $comment= Comment::paginate(5);
        return view('posts.index',compact('data','comment'))
            ->with('i', (request()->input('page', 1) - 1) * 5);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('posts.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Post $post, Comment $comment)
    {
        //
        $request->validate([
            'player_name' => 'required',
            'role' => 'required',
            'age'=>'required',
        ]);

        $post= Post::create($request->all());
        //$post=Post::latest();
        $id=$post->id;
         Comment::create([
         'post_id' => $id,
         'comments' => request('comment'),
     ]);

        // $post=Post::get('id');
        return redirect()->route('posts.show',$id)
            ->with('success','Post created successfully.');



    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Post  $post
     * @param \App\Models\Comment $comment
     * @return \Illuminate\Http\Response
     */
    public function show(Post $post, Comment $comment)
    {
        //
        return view('posts.show',compact('post', 'comment'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function edit(Post $post)
    {
        //
        //$post->show();
       return view('posts.edit',compact('post'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Post  $post
     * @param  \App\Models\Comment $comment
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Post $post, Comment $comment)
    {

        $request->validate([
            'player_name' => 'required',
            'role' => 'required',
            'age'=>'required',
        ]);


//        $comment->update($request->find('$id'));
//        $comment = Comment::find($post->id);
//        $comment->comments =request('comment');
//
//        $comment->save();
        $post->update($request->all());
        $id=$post->id;
        Comment::where('post_id', $id)
            ->update(['comments' => request('comments')]);
        return redirect()->route('posts.show',$id)
            ->with('success','Post created successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function destroy(Post $post)
    {
        //
        $post->delete();

        return redirect()->route('posts.index')
            ->with('success','Post deleted successfully');
    }
}
