<?php

namespace App\Http\Controllers;
use App\Models\User;
use DebugBar\DebugBar;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
class UserController extends Controller
{

    public function uploadAvatar(Request $request){
        if($request->hasFile('images'))
        {
            User::uploadAvatar($request->images);
            return redirect()->back();
        }

        return redirect()->back();
    }


    //
    public function index()
    {
        $user=new User();

        //var_dump($user);
//
//        $data=['name'=>'kamal',
//            'email'=>'kamal@gmail.com',
//            'password'=>'password'
//            ];
//        User::create($data);

//        $user->name='mahi';
//        $user->email='mahi@gmail.com';
//        $user->password=bcrypt('password');
//        $user->save();

//          $user=User::all();
//          return $user;

//          User::where('id',2)->delete();

//            User::where('id',4)->update(['name'=>'mahinesh']);

//        DB::insert('insert into users(name,email, password)
//                    values(?,?,?)',[
//                        'mahi', 'mahi@gmail.com', 'password']
//);
//        $users=DB::select('select * from users');
///       return $users;
///
//        DB::update('update users set name=? where id=1', ['mahinesh']);

//        DB::delete('delete from users where id=1');


        return view('home');
    }
}
