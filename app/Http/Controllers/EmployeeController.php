<?php

namespace App\Http\Controllers;
use DataTables;
use Illuminate\Http\Request;
use App\Models\Employee;
class EmployeeController extends Controller
{
    //
    public function index(Request $request, $id=null)
    {
        if ($request->ajax()) {
            $data = Employee::select('*');
            return Datatables::of($data)
                ->make(true);
        }
        //$id=2;

        return $id?Employee::find($id):Employee::all();
//        return view('employee');
    }
    public function add(){
//        $add=new Employee;
//        $add->name=$request->name;
//        $add->email=$request->email;
//        $add->phone=$request->phone;
//        $add->salary=$request->salary;
//        $add->department=$request->department;
//        $res=$add->save();
//        if($res)
//        {
//            return ["result"=>"Data has been saved"];
//        }
//        else{
//            return ["result"=>"Data has not been saved"];
//        }
        return ["result"=>"Data has been saved"];
    }
}
