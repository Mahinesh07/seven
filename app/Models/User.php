<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Storage;
use App\Models\Team;
use App\Http\Controllers\UserController;
use Illuminate\Database\Eloquent\Relations\HasMany ;

class User extends Authenticatable
{
    use HasFactory, Notifiable;

    protected $with=['profile'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'avatar',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

//    public function setPasswordAttribute($password){
//        $this->attributes['password']=bcrypt($password);
//    }
//
//    public function getNameAttribute($name){
//        return 'My name is : '. ucfirst($name);
//    }
    public static function uploadAvatar($images)
    {

        $filename = $images->getClientOriginalName();
        (new self())->deleteOldImage();
        $images->storeAs('images', $filename, 'public');
        auth()->user()->update(['avatar' => $filename]);

}

    protected function deleteOldImage()
    {
        if ($this->avatar) {
            //dd('/storage/app/public/image/'.auth()->user()->avatar);
            Storage::delete('/public/images/' . $this->avatar);
        }
    }



}
