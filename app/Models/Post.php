<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    use HasFactory;
    protected $fillable=[
        'player_name',
        'role',
        'age',
    ];

    public function profile(){
        return $this->hasOne(Profile::class);
    }

    public function team(){
        return $this->hasMany(Team::class, player_id,id);
    }

    public function comment(){
        return $this->hasMany(Comment::class);
    }
}
