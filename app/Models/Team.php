<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Team extends Model
{
    protected $guarded=[];

    public function team(){
        return $this->belongsTo(Post::class, 'user_id');
    }

    use HasFactory;
}
