<?php


use App\Http\Controllers\UserController;
use App\Http\Controllers\EmployeeController;
use App\Http\Controllers\SurveyFormController;
use Illuminate\Support\Facades\Route;
use App\Models\User;
use Barryvdh\Debugbar\Facade as Debugbar;
use Illuminate\Support\Facades\Log;
use App\Http\Controllers\PostController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {


    Log::debug('Log is activated');

    return view('welcome');
});
Route::get('/users',[UserController::class,'index']);

//Route::get('/users',function(){
//    try{
//        throw new Exception('This is a new exception');
//    }
//    catch(\Exception $e){
//        DebugBar::addException($e);
//    }
//    $user=User::get();
//    debugBar::warning('warning');
//    debugBar::info($user);
//    debugBar::addMessage('message','Hi all');
//    return view('welcome');
//});

//Route::get('/users','UserController@index');

Route::get('/user', function () {
    return view('home');
});

Route::post('/upload', [UserController::class, 'uploadAvatar']);
    //dd($request->file('images');
    //dd($request->all());
    //dd($request->hasfile('images'));
    //dd($request->images);
//    $request->images->store('images','public');
//    return 'uploadedd';


Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::get('employee', [EmployeeController::class, 'index']);

Route::get('/surveyform', [SurveyFormController::class, 'index']);

Route::get('editing', function(){
    return view('editted');
});


Route::resource('posts', PostController::class);

Route::post('add', [EmployeeController::class, 'add']);
